package com.sndi.springbootTraining.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sndi.springbootTraining.entities.Acheteur;

public interface AcheteurRepositry extends JpaRepository<Acheteur, Long>{

}

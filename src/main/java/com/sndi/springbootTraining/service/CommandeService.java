package com.sndi.springbootTraining.service;

import java.util.Date;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.sndi.springbootTraining.entities.Acheteur;
import com.sndi.springbootTraining.entities.Article;
import com.sndi.springbootTraining.entities.Commande;
import com.sndi.springbootTraining.repositories.AcheteurRepositry;
import com.sndi.springbootTraining.repositories.ArticleRepository;
import com.sndi.springbootTraining.repositories.CommandeRepository;

@Service
public class CommandeService {

	private final ArticleRepository articleRepository;
	private AcheteurRepositry acheteurRepositry;
	private CommandeRepository commandeRepository ;
	
	public CommandeService(
			ArticleRepository articleRepository, 
			AcheteurRepositry acheteurRepositry,
			CommandeRepository commandeRepository
			) {
		
		this.articleRepository = articleRepository;
		this.acheteurRepositry = acheteurRepositry;
		this.commandeRepository = commandeRepository;
	}
	
	
	public Long commander(CommandeDto dto){
		
		Commande commande =  new Commande();
		Long idUseur = Long.valueOf(dto.idUser);
		commande.setAcheteur(acheteurRepositry.findById(idUseur).get());
		Set<Article> articles =  articleRepository.findByIdIn(dto.articles);
		commande.setArticles(articles);
		commande.setDate(new Date());
		commandeRepository.save(commande);
		return commande.getId();
		
		
	}
}

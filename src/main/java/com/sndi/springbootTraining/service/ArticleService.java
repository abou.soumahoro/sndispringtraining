package com.sndi.springbootTraining.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.sndi.springbootTraining.entities.Article;
import com.sndi.springbootTraining.entities.User;
import com.sndi.springbootTraining.entities.Vendeur;
import com.sndi.springbootTraining.repositories.ArticleRepository;
import com.sndi.springbootTraining.repositories.UserRepository;

@Service
public class ArticleService {

	private final ArticleRepository articleRepository;
	private final UserRepository userRepository;
	Logger logger = LoggerFactory.getLogger(ArticleService.class);
	
	public ArticleService(ArticleRepository articleRepository, UserRepository userRepository) {
		this.articleRepository = articleRepository;
		this.userRepository = userRepository;
	}
	
	public Article createArticle(ArticleDto dto)
	{
		Article article = new Article();
		logger.info("*************"+  dto.idUser);
		User vendeur = userRepository.findById(Long.valueOf(dto.idUser)).get();
		
		article.setVendeur((Vendeur) vendeur);
		article.setDescription(dto.description);
		article.setName(dto.name);
		article.setPrix(dto.prix);
		
		return articleRepository.save(article);
	}
}

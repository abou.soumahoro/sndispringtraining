package com.sndi.springbootTraining.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sndi.springbootTraining.service.CommandeDto;
import com.sndi.springbootTraining.service.CommandeService;

@RestController
public class CommandController {

	private final CommandeService commandeService;
	
	
	public CommandController(CommandeService commandeService) {
		this.commandeService = commandeService;
	}
	
	@PostMapping("/commande")
	public Long commander(@RequestBody CommandeDto dto) {
		return commandeService.commander(dto);
	}
}

package com.sndi.springbootTraining.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sndi.springbootTraining.entities.Article;
import com.sndi.springbootTraining.service.ArticleDto;
import com.sndi.springbootTraining.service.ArticleService;

@RestController
public class ArticleController {

	private final ArticleService articleService;
	
	public ArticleController(ArticleService articleService) {
		this.articleService = articleService;
	}
	
	@PostMapping("/article")
	public Article createArticle(@RequestBody ArticleDto articleDto) {
		
		return articleService.createArticle(articleDto);
	}
}
